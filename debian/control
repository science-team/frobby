Source: frobby
Maintainer: Debian Math Team <team+math@tracker.debian.org>
Uploaders: Doug Torrance <dtorrance@piedmont.edu>
Section: math
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Arch: g++ (>= 4:7), libgmp-dev, texlive-latex-base
Build-Depends-Indep: doxygen
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/math-team/frobby
Vcs-Git: https://salsa.debian.org/math-team/frobby.git
Homepage: https://github.com/Macaulay2/frobby
Rules-Requires-Root: no

Package: frobby
Architecture: any
Multi-Arch: foreign
Depends: ${misc:Depends}, ${shlibs:Depends}
Suggests: 4ti2
Description: Computations with monomial ideals
 Frobby is a software system and project for computations with monomial ideals.
 Frobby is free software and it is intended as a vehicle for computational and
 mathematical research on monomial ideals.
 .
 The current functionality includes Euler characteristic, Hilbert series,
 maximal standard monomials, combinatorial optimization on monomial ideals,
 primary decomposition, irreducible decomposition, Alexander dual, associated
 primes, minimization and intersection of monomial ideals as well as the
 computation of Frobenius problems (using 4ti2) with very mmlarge numbers.
 Frobby is also able to translate between formats that can be used with several
 different computer systems, such as Macaulay 2, Monos, 4ti2, CoCoA4 and
 Singular. Thus Frobby can be used with any of those systems.

Package: libfrobby0
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${misc:Depends}, ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: Computations with monomial ideals (shared library)
 Frobby is a software system and project for computations with monomial ideals.
 Frobby is free software and it is intended as a vehicle for computational and
 mathematical research on monomial ideals.
 .
 The current functionality includes Euler characteristic, Hilbert series,
 maximal standard monomials, combinatorial optimization on monomial ideals,
 primary decomposition, irreducible decomposition, Alexander dual, associated
 primes, minimization and intersection of monomial ideals as well as the
 computation of Frobenius problems (using 4ti2) with very large numbers. Frobby
 is also able to translate between formats that can be used with several
 different computer systems, such as Macaulay 2, Monos, 4ti2, CoCoA4 and
 Singular. Thus Frobby can be used with any of those systems.
 .
 This package contains the shared library.

Package: libfrobby-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: libfrobby0 (=${binary:Version}), libgmp-dev, ${misc:Depends}
Recommends: libfrobby-doc (=${binary:Version})
Description: Computations with monomial ideals (development tools)
 Frobby is a software system and project for computations with monomial ideals.
 Frobby is free software and it is intended as a vehicle for computational and
 mathematical research on monomial ideals.
 .
 The current functionality includes Euler characteristic, Hilbert series,
 maximal standard monomials, combinatorial optimization on monomial ideals,
 primary decomposition, irreducible decomposition, Alexander dual, associated
 primes, minimization and intersection of monomial ideals as well as the
 computation of Frobenius problems (using 4ti2) with very large numbers. Frobby
 is also able to translate between formats that can be used with several
 different computer systems, such as Macaulay 2, Monos, 4ti2, CoCoA4 and
 Singular. Thus Frobby can be used with any of those systems.
 .
 This package contains the development tools.

Package: libfrobby-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends}
Description: Computations with monomial ideals (library documentation)
 Frobby is a software system and project for computations with monomial ideals.
 Frobby is free software and it is intended as a vehicle for computational and
 mathematical research on monomial ideals.
 .
 The current functionality includes Euler characteristic, Hilbert series,
 maximal standard monomials, combinatorial optimization on monomial ideals,
 primary decomposition, irreducible decomposition, Alexander dual, associated
 primes, minimization and intersection of monomial ideals as well as the
 computation of Frobenius problems (using 4ti2) with very large numbers. Frobby
 is also able to translate between formats that can be used with several
 different computer systems, such as Macaulay 2, Monos, 4ti2, CoCoA4 and
 Singular. Thus Frobby can be used with any of those systems.
 .
 This package contains the library documentation.
